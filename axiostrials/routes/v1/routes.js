const express = require("express");
const bodyParser = require("body-parser");
const { doUpload } = require("./controllers/docs.js");
const { doSendSms } = require("./controllers/smsindiahub.js");
const router = express.Router();

router.get("/docs", doUpload);
router.get("/sms", doSendSms);

module.exports = router;
