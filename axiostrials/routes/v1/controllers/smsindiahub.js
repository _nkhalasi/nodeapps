const axios = require("axios");

const smsindiahub = {
  creds: {
    user: "manojit",
    password: "dummy",
  },
  others: {
    url: "http://cloud.smsindiahub.in/vendorsms/pushsms.aspx",
    kycWelcomeMsg:
      "Dear ##customer_name##, Welcome to Vayana Network. To share your KYC documents please click https://wa.me/##vayana_whatsapp_number##?text=##start_msg##",
    senderId: "VAYNET",
    fl: 0,
    gwid: 2,
  },
};

const doSendSms = async (req, res) => {
  res.send(
    JSON.stringify(
      await sendSms("Acme Inc.", "+911212121212", "+91-2323232323")
    )
  );
};

function Message(msg) {
  function handlePlaceHolders(placeHolders) {
    const keys = Object.keys(placeHolders);
    const finalMsg = keys.reduce((acc, key) => {
      return acc.replace(key, placeHolders[key]);
    }, msg);
    return finalMsg;
  }
  return handlePlaceHolders;
}

const kycWelcomeMsg = Message(smsindiahub.others.kycWelcomeMsg);

const sendSms = async (orgName, userMobile, vayanaWaNumber) => {
  const targetMobile = userMobile.replace("+91-", "91").replace("+91", "91");
  const vaynetMobile = vayanaWaNumber
    .replace("+91-", "91")
    .replace("+91", "91");
  const msg = kycWelcomeMsg({
    "##customer_name##": orgName,
    "##vayana_whatsapp_number##": vaynetMobile,
    "##start_msg##": encodeURIComponent("start kyc"),
  });

  try {
    const reqConfig = {
      method: "GET",
      url: smsindiahub.others.url,
      params: {
        user: smsindiahub.creds.user,
        password: smsindiahub.creds.password,
        msg: msg,
        sid: smsindiahub.others.senderId,
        msisdn: targetMobile,
        fl: smsindiahub.others.fl,
        gwid: smsindiahub.others.gwid,
      },
    };

    const response = await axios(reqConfig);
    console.log(response);
    return { success: 1, message: response.data.ErrorMessage };
  } catch (error) {
    console.log("Unable to send SMS ", error.message);
    return { success: 0, message: "Unable to send SMS: " + error.message };
  }
};

module.exports = { doSendSms };
