const stream = require("stream");
const axios = require("axios");
const FormData = require("form-data");
const pan_image_b64 = require("../../../data/panimage");

const creds = {
  handle: "groot-sysadmin@vayana.com",
  password: "Abcd12345678!",
  handleType: "email",
};

const authenticateVayana = async () => {
  try {
    let reqConfig = {
      method: "POST",
      url: "https://sandbox.services.vayananet.com/theodore/apis/v1/authtokens",
      headers: { "Content-Type": "application/json" },
      data: creds,
    };
    const authResponse = await axios(reqConfig);
    console.log("----------------------------");
    console.log(authResponse.data);
    console.log("----------------------------");
    if (authResponse.data.data.token) {
      var token = authResponse.data.data.token;
      var userId = authResponse.data.data.userId;
      return { token: token, userId: userId, success: 1 };
    } else {
      console.log("1Vayana authentication failed ", authResponse);
      return { token: null, userId: null, success: 0 };
    }
  } catch (error) {
    console.log("2Vayana authentication failed ", error.message);
    return { token: null, userId: null, success: 0 };
  }
};

function bufferToReadableStream(bufferData) {
  const readable = new stream.Readable();
  readable._read = () => {}; // _read is required but you can noop it
  readable.push(bufferData);
  readable.push(null);
  return readable;
}

const doUpload = async (req, res) => {
  try {
    const authResult = await authenticateVayana();
    if (authResult.success == 0) {
      console.log("Error while posting data because of authorization error");
      return { success: 0 };
    }
    const docMetadata = {
      docCategory: "bns",
      files: { doc: "gst.pdf" },
      attributes: {},
      userId: "3026fdf6-8e0c-4bb4-9601-460d67ed9b7f",
      mobile: "+91-9167152777",
    };
    const formData = new FormData();
    formData.append("data", JSON.stringify(docMetadata));
    formData.append("doc", Buffer.from(pan_image_b64, "base64"), {
      filename: "gst.pdf",
    });
    console.log(formData);
    console.log("=================================");
    const reqConfig = {
      url:
        "https://sandbox.apps.vayananet.com/groot/apis/v1/kyc/2d62bc2f-936e-4b45-aa28-0d352e573e2e/docs/gst",
      method: "POST",
      data: formData,
      headers: {
        "Content-Type": formData.getHeaders()["content-type"],
        Authorization: "Bearer " + authResult.token,
      },
    };
    const response = await axios(reqConfig);
    console.log("UPLOAD DOC CALL TO VAYANA RESPONSE: ", response);
  } catch (error) {
    console.log("Error while posting doc to vayana. ", error);
  }
};

module.exports = { doUpload };
