const express = require("express");
const app = express();
const v1routes = require("./routes/v1/routes.js");
const env = process.env.NODE_ENV || "test";

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use("/apis/v1", v1routes);

const server = app.listen("8081", function () {
  console.log(
    "Express server listening on port " +
      server.address().port +
      "  with ENV - " +
      env
  );
});

process.on("uncaughtException", (err) => {
  try {
    console.log(err);
    console.log(err.stack.split("\n"));
  } catch (e) {
    console.error(err);
  }
});

let exitHandler = (options, err) => {
  console.log("Goodbye! , removed the cron job");
  process.exit();
};

process.on("exit", exitHandler.bind(null, { cleanup: true }));
process.on("SIGINT", exitHandler.bind(null, { exit: true }));
process.on("uncaughtException", exitHandler.bind(null, { exit: true }));

module.exports = app;
