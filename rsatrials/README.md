### Containerization

```
docker build --force-rm --rm --compress --no-cache --tag "rsatrials" .
docker run --rm -p 8081:8018 -t --name=rsatrials rsatrials:latest
```
