const crypto = require("crypto");

const appKey = "nqGHWLiH1PKrh1uAnEGvIX24mNEFOZ8O3CVOF2U20kw=";
const key = Buffer.from(appKey, "base64");
console.log(key.length);

const cbcAlgorithm = "aes-256-cbc";
const ebcAlgorithm = "aes-256-ecb";

// const key = crypto.randomBytes(32);
// console.log(Buffer.from(key).toString("base64"));

const cbcIv = crypto.randomBytes(16);
const nullIv = Buffer.alloc(0);

function cbcEncrypt(text, key, iv) {
  let cipher = crypto.createCipheriv(cbcAlgorithm, key, iv);
  cipher.setAutoPadding(true);
  let encrypted = cipher.update(text);
  encrypted = Buffer.concat([encrypted, cipher.final()]);
  return {
    iv: iv.toString("base64"),
    encryptedData: encrypted.toString("base64"),
  };
}

function cbcDecrypt(text, key) {
  let iv = Buffer.from(text.iv, "base64");
  let encryptedText = Buffer.from(text.encryptedData, "base64");
  let decipher = crypto.createDecipheriv(cbcAlgorithm, key, iv);
  let decrypted = decipher.update(encryptedText);
  decrypted = Buffer.concat([decrypted, decipher.final()]);
  return decrypted.toString();
}

function ecbEncrypt(text, key) {
  let cipher = crypto.createCipheriv(ebcAlgorithm, key, nullIv);
  cipher.setAutoPadding(true);
  let encrypted = cipher.update(text);
  encrypted = Buffer.concat([encrypted, cipher.final()]);
  return {
    encryptedData: encrypted.toString("base64"),
  };
}

function ecbDecrypt(text, key) {
  let encryptedText = Buffer.from(text.encryptedData, "base64");
  let decipher = crypto.createDecipheriv(ebcAlgorithm, key, nullIv);
  decipher.setAutoPadding(true);
  let decrypted = decipher.update(encryptedText);
  decrypted = Buffer.concat([decrypted, decipher.final()]);
  return decrypted.toString();
}

console.log("------------CBC---------");
var hw = cbcEncrypt("575757", key, cbcIv);
console.log(hw);
console.log(cbcDecrypt(hw, key));

console.log("------------EBC---------");
var hw = ecbEncrypt("575757", key);
console.log(hw);
console.log(ecbDecrypt(hw, key));
