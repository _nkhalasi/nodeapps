const crypto = require("crypto");
const fs = require("fs");
const path = require("path");

function sha256Hash(data) {
  return crypto.createHash("sha256").update(data).digest("base64");
}

function messageHash(secret, userId, token) {
  const message = "vlms :: " + secret + " :: " + token + " :: " + userId;
  const msgHash = sha256Hash(message);
  console.log("Message: " + message);
  console.log("Message Hash: " + msgHash);
  return msgHash;
}

const privateKeyPem = fs
  .readFileSync(path.join(__dirname, "private.nopass.pem"))
  .toString()
  .trim();

function sign(secret, userId, token, privateKey) {
  const signer = crypto.createSign("sha256WithRSAEncryption");
  signer.update(Buffer.from(messageHash(secret, userId, token), "base64"));
  return signer.sign(privateKey, "base64");
}

const publicKeyPem = fs
  .readFileSync(path.join(__dirname, "public.pem"))
  .toString("ascii")
  .trim();

function verify(secret, userId, token, signature, publicKey) {
  const verifier = crypto.createVerify("sha256");
  verifier.update(Buffer.from(messageHash(secret, userId, token), "base64"));
  return verifier.verify(publicKey, signature, "base64");
}

const secret = "This is a secret";
const userId = "namesakes";
const token = "5600f6c2-24c1-47b7-9cc8-53665e5a327a";

const signature = sign(secret, userId, token, privateKeyPem);
console.log("Signature: " + signature);

console.log(
  "Verification: " + verify(secret, userId, token, signature, publicKeyPem)
);

// console.log(crypto.getHashes());
// console.log(crypto.getCiphers());

//openssl genpkey -algorithm RSA -aes256 -out private.pem
//openssl rsa -in private.pem -pubout -outform PEM -out public.pem
//openssl rsa -in private.pem -out private.nopass.pem
//echo -n "some secret" | openssl dgst -sha256 -sign private.nopass.pem  > helloTest
//echo -n "some secret" | openssl dgst -sha256 -verify public.pem -signature helloTest
//openssl base64 -in helloTest -out sha256.b64
//echo "x-vayana-tyntec-token: $(cat sha256.b64 | paste -sd "" -)" > sha256.b64
//curl -X POST --data '{"name": "Naresh"}' http://localhost:8081/apis/v1/messages --header "Content-Type:application/json" -H @token_header.txt
