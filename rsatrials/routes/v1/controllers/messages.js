const crypto = require("crypto");
const fs = require("fs");
const path = require("path");

// const privateKeyPem = fs
//   .readFileSync(path.join(__dirname, "../../../private.nopass.pem"))
//   .toString()
//   .trim();

// function signToken(token, privateKey) {
//   const signer = crypto.createSign("sha256");
//   signer.update(token);
//   return signer.sign(privateKey, "base64");
// }

const publicKeyPem = fs
  .readFileSync(path.join(__dirname, "../../../public.pem"))
  .toString("ascii")
  .trim();

function verifyToken(token, signature, publicKey) {
  const verifier = crypto.createVerify("sha256");
  verifier.update(token);
  return verifier.verify(publicKey, signature, "base64");
}

function receiveMessage(req, res) {
  console.log(req.body);
  console.log(req.headers);

  let tokenSignature = req.headers["x-vayana-tyntec-token"];
  console.log(verifyToken("some secret", tokenSignature, publicKeyPem));

  res.send("Received ${req.ip}");
}

function listMessages(req, res) {
  res.send("You requested for all messages");
}

module.exports = { receiveMessage, listMessages };

//openssl genpkey -algorithm RSA -aes256 -out private.pem
//openssl rsa -in private.pem -pubout -outform PEM -out public.pem
//openssl rsa -in private.pem -out private.nopass.pem
//echo -n "some secret" | openssl dgst -sha256 -sign private.nopass.pem  > helloTest
//echo -n "some secret" | openssl dgst -sha256 -verify public.pem -signature helloTest
//openssl base64 -in helloTest -out sha256.b64
//echo "x-vayana-tyntec-token: $(cat sha256.b64 | paste -sd "" -)" > sha256.b64
//curl -X POST --data '{"name": "Naresh"}' http://localhost:8081/apis/v1/messages --header "Content-Type:application/json" -H @token_header.txt
