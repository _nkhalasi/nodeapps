const express = require("express");
const bodyParser = require("body-parser");
const msgs = require("./controllers/messages.js");
const router = express.Router();

router.post("/messages", msgs.receiveMessage);
router.get("/messages", msgs.listMessages);

module.exports = router;
